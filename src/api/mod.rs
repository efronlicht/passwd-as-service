#[cfg(test)]
mod tests;

use super::{
    model::{Group, User},
    Query, Resource,
};

use std::io::Result;
/// GET /users Return a list of all users on the system, as defined in the /etc/passwd file.
pub fn get_all_users(users: impl Resource<User>) -> Result<Vec<User>> {
    Ok(users.load()?.values().cloned().collect())
}

///GET /groups/query[?name=<nq>][&gid=<gq>][&member=<mq1>[&member=<mq2>][&. ..]] Return a list of groups matching all of the specified query fields. The bracket notation indicates that any of the following query parameters may be supplied: - name - gid - member (repeated)
pub fn get_matching_users(query: impl Query<User>, users: impl Resource<User>) -> Result<Vec<User>> {
    Ok(query
        .matching_elements(users.load()?.values())
        .into_iter()
        .cloned()
        .collect())
}

///GET /users/<uid>/groups Return all the groups for a given user.

pub fn get_groups_for_user(groups: impl Resource<Group>, users: impl Resource<User>, uid: u64) -> Option<Vec<Group>> {
    let username = users.load().ok()?.get(&uid)?.name.to_string();
    Some(
        groups
            .load()
            .ok()?
            .values()
            .filter(|g| g.members.iter().any(|member| member == &username))
            .cloned()
            .collect(),
    )
}
///GET /users/<uid> Return a single user with <uid>. Return 404 if <uid> is not found.
///Example Response: {“name”: “dwoodlins”, “uid”: 1001, “gid”: 1001, “comment”: “”, “home”: “/home/dwoodlins”, “shell”: “/bin/false”}
pub fn get_user(users: impl Resource<User>, uid: u64) -> Option<User> {
    users.load().ok()?.get(&uid).and_then(|t| Some(t.clone()))
}

/// GET /groups/<gid> Return a single group with <gid>. Return 404 if <gid> is not found.
pub fn get_group(groups: impl Resource<Group>, gid: u64) -> Option<Group> {
    groups.load().ok()?.get(&gid).and_then(|t| Some(t.clone()))
}
/// GET /groups
pub fn get_all_groups(groups: impl Resource<Group>) -> Result<Vec<Group>> {
    Ok(groups.load()?.values().cloned().collect())
}
/// GET /groups/query[?name=<nq>][&gid=<gq>][&member=<mq1>[&member=<mq2>][&. ..]]
pub fn get_matching_groups(query: impl Query<Group>, groups: impl Resource<Group>) -> Result<Vec<Group>> {
    Ok(query
        .matching_elements(groups.load()?.values())
        .into_iter()
        .cloned()
        .collect())
}
